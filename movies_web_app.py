import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode
from types import *

application = Flask(__name__)
app = application

def isfloat(x):
    try:
        a = float(x)
    except ValueError:
        return False
    else:
        return True

def isint(x):
    try:
        a = float(x)
        b = int(a)
    except ValueError:
        return False
    else:
        return a == b

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname
    
def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating Decimal(4,2), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    cnx = ''
    try:
        year = request.form['year']
        title = request.form['title']
        director = request.form['director']
        actor = request.form['actor']
        release_date = request.form['release_date']
        rating = request.form['rating']
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print('Movie ' +  title + ' could not be inserted - ' + exp)
        messages = [str(exp)]
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    if not year:
        messages = ['No year provided']
    elif not title:
        messages = ['No title provided']
    elif not director:
        messages = ['No director provided']
    elif not actor:
        messages = ['No actor provided']
    elif not release_date:
        messages = ['No release date provided']
    elif not rating:
        messages = ['No rating provided']
    elif not isint(year):
        messages = ['Invalid input: year must be integer']    
    elif not isfloat(rating):
        messages = ['Invalid input: rating must be a float or an integer']
    else:
        cur = cnx.cursor()
        rating = str(float(rating))
        #check for existing movie title
        sql = """ SELECT title from movies WHERE LOWER(title) = '%s' """ % (title.lower())
        cur.execute(sql)
        results = cur.fetchall()
        messages = []

        if not results:
            try:
                cur.execute(""" INSERT INTO movies (year, title, director, actor, release_date, rating) values (%s, %s, %s , %s, %s, %s) """, (year, title, director, actor, release_date, rating))
                cnx.commit()
                messages = ['Movie ' + title + ' successfully inserted']
                
            except Exception as exp:
                messages = ['Movie ' + title + ' could not be inserted - ' + exp]        
        else:
            messages = ['Movie ' + title + ' already exists']
    
    return render_template('index.html', messages=messages)

@app.route('/update_movie', methods=['POST'])
def update_movie():

    #get input
    cnx = ''
    try:
        year = request.form['year']
        title = request.form['title']
        director = request.form['director']
        actor = request.form['actor']
        release_date = request.form['release_date']
        rating = request.form['rating']
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    if not year:
        messages = ['No year provided']
    elif not title:
        messages = ['No title provided']
    elif not director:
        messages = ['No director provided']
    elif not actor:
        messages = ['No actor provided']
    elif not release_date:
        messages = ['No release date provided']
    elif not rating:
        messages = ['No rating provided']
    elif not isint(year):
        messages = ['Invalid input: year must be integer']    
    elif not isfloat(rating):
        messages = ['Invalid input: rating must be a float or an integer']
    else:
        cur = cnx.cursor()
        rating = str(float(rating))
        #check for existing movie title
        sql = """ SELECT title from movies WHERE LOWER(title) = '%s' """ % (title.lower())
        cur.execute(sql)
        results = cur.fetchall()
        messages = []

        if not results:
            messages = ['Movie ' + title + ' does not exist']
        else:
            try:
                cur.execute (""" UPDATE movies SET year=%s, title=%s, director=%s, actor=%s, release_date=%s, rating=%s WHERE title=%s """, (year, title, director, actor, release_date, rating, title))
                cnx.commit()
                messages = ['Movie ' + title + ' successfully updated']
            except Exception as exp:
                messages = ['Movie ' + title + ' could not be updated - ' + exp]

    return render_template('index.html', messages=messages)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    #get input
    cnx = ''
    try:
        title = request.form['delete_title']
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password, host=hostname, database=db)

    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    #check for existing movie title
    sql = " SELECT title from movies WHERE LOWER(title) = '%s' " % (title.lower())
    cur.execute(sql)
    results = cur.fetchall()
    messages = []

    if not results:
        messages = ['Movie ' + title + ' does not exist']
    else:
        try:

            cur.execute ("DELETE from movies WHERE LOWER(title)= '%s' " % title.lower())
            cnx.commit()
            messages = ['Movie ' + title + ' successfully deleted']
        except Exception as exp:
            messages = ['Movie ' + title + ' could not be deleted - ' + exp]

    return render_template('index.html', messages=messages)

@app.route('/search_movie', methods=['GET'])
def search_movie():

    cnx = ''

    try:
        actor = request.args.get('search_actor')
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    #check for existing movie title
    sql = "SELECT title, year, actor from movies WHERE LOWER(actor) = '%s' " % (actor.lower())
    cur.execute(sql)
    results = cur.fetchall()
    messages = []

    if not results:
        messages = ['No movies found for actor ' + actor]
    else:
        #for row in results:
        messages += ['Title: ' + row[0] + ' Year: ' + row [1] + ' Actor: ' + row[2] for row in results] #check?

    #message = 'hi'

    return render_template('index.html', messages=messages)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    cnx = ''

    try:
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    cur = cnx.cursor()
    sql = "SELECT rating from movies"
    cur.execute(sql)
    results = cur.fetchall()
    messages = []
    
    if not results:
        messages = ['No movies found']

    else:

        try:
            #get lowest rating
            highest_ratings =  [float(str(''.join(str(r) for r in result))) for result in results]
            highest_ratings.sort()
            highest_ratings.reverse()
            highest_rating = highest_ratings[0]
            rating = [highest_rating]

            #get movie info from movies with lowest rating
            sql = """ SELECT title, year, actor, director, rating from movies WHERE rating = '%s' """ % (highest_rating)
            cur.execute(sql)
            results = cur.fetchall()
            messages = [(str( str(' '.join(str(r) for r in result)))) for result in results]

        except Exception as exp:
            messages = [exp]

    return render_template('index.html', messages=messages)
    
@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    cnx = ''

    try:
        db, username, password, hostname = get_db_creds()
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    cur = cnx.cursor()
    sql = "SELECT rating from movies"
    cur.execute(sql)
    results = cur.fetchall()
    messages = []
    
    if not results:
        messages = ['No movies found']

    else:

        try:
            #get lowest rating
            lowest_ratings =  [float(str(''.join(str(r) for r in result))) for result in results]
            lowest_ratings.sort()
            lowest_rating = lowest_ratings[0]
            rating = [lowest_rating]

            #get movie info from movies with lowest rating
            sql = """ SELECT title, year, actor, director, rating from movies WHERE rating = '%s' """ % (lowest_rating)
            cur.execute(sql)
            results = cur.fetchall()
            messages = [(str( str(' '.join(str(r) for r in result)))) for result in results]

        except Exception as exp:
            messages = [exp]

    return render_template('index.html', messages=messages)

@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    create_table()

    messages = ['message']
    print("Entries: %s" % messages)
    return render_template('index.html', messages=messages)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
